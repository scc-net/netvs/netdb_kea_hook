// Kea
#include <hooks/hooks.h>
#include <dhcpsrv/srv_config.h>
#include <dhcpsrv/cfg_subnets4.h>
#include <asiolink/io_address.h>
#include <util/triplet.h>
#include <log/macros.h>

// pq
#include <pqxx/pqxx>

// misc
#include <boost/shared_ptr.hpp>
#include <boost/algorithm/hex.hpp>

using namespace isc::dhcp;
using namespace isc::hooks;
using namespace isc::data;

namespace netdb_kea {
    isc::log::Logger netdb_logger("netdb-kea");
}

using namespace netdb_kea;

const char *NETDB_OPTION_CONTEXT = "netdb_opt_transport";
const char *NETDB_DBCONN_CONTEXT = "netdb_dbcon_transport";
const char *NETDB_FAKE_HOST_CONTEXT = "netdb_fake_host_transport";

const char *ERR_DB_CONN = "Can't open database";
const char *ERR_SQL_WITH_OPTS = "SQL error: %0\n Query was: %1\n SQL state: %2\n";

SrvConfigPtr config;
std::string db_conn;

void extract_options(OptionCollectionPtr &tp, const pqxx::row &row) {
    auto opts = row["out_lease_options"].as_array();
    std::pair<pqxx::array_parser::juncture, std::string> elem;
    uint32_t i = 0;
    uint16_t opt_code;
    do {
        elem = opts.get_next();
        if (elem.first == pqxx::array_parser::string_value) {
            if (i++ % 2 == 0) {
                opt_code = std::stoul(elem.second.erase(0, 2), nullptr, 16);
            } else {
                std::vector<uint8_t> out;
                boost::algorithm::unhex(elem.second.begin() + 2, elem.second.end(),
                                        std::back_inserter(out));
                auto opt_pair = std::make_pair(opt_code,
                                               OptionPtr(new Option(Option::V4, opt_code, out)));
                tp->insert(opt_pair);
            }
        }
    } while (elem.first != pqxx::array_parser::done);
}

extern "C" {
int version() {
    return (KEA_HOOKS_VERSION);
}

int multi_threading_compatible() {
    return true;
}

int load(LibraryHandle &handle) {
    ConstElementPtr db_conn_string = handle.getParameter("pg_connect");
    if (!db_conn_string) {
        return (1);
    }
    if (db_conn_string->getType() != Element::string) {
        return (1);
    }
    db_conn = db_conn_string->stringValue();
    try {
        pqxx::connection C(db_conn);
        C.disconnect();
    } catch (const pqxx::broken_connection &) {
        netdb_logger.warn(ERR_DB_CONN);
    }
    return (0);
}

int context_create(CalloutHandle &handle) {
    const auto tp = boost::make_shared<OptionCollection>();
    handle.setContext(NETDB_OPTION_CONTEXT, tp);
    const boost::shared_ptr<pqxx::connection> conn_ptr(new pqxx::connection(db_conn));
    pqxx::work txn{*conn_ptr};
    txn.exec0("call dhcpd.api_set_clnt_sess_params();");
    handle.setContext(NETDB_DBCONN_CONTEXT, conn_ptr);
    return (0);
}

int context_destroy(CalloutHandle &handle) {
    try {
        HostPtr host;
        handle.getContext(NETDB_FAKE_HOST_CONTEXT, host);
        if (config->getCfgHosts() != nullptr) {
            config->getCfgHosts()->del(host->getIPv4SubnetID(), host->getIPv4Reservation());
        }
        boost::shared_ptr<pqxx::connection> c;
        handle.getContext(NETDB_DBCONN_CONTEXT, c);
        if (c != nullptr && c->is_open()) {
            c->disconnect();
        }
    } catch (const NoSuchCalloutContext &) {
    }
    return (0);
}

int dhcp4_srv_configured(CalloutHandle &handle) {
    handle.getArgument("server_config", config);
    return (0);
}

int pkt4_receive(CalloutHandle &handle) {
    try {
        boost::shared_ptr<pqxx::connection> c;
        handle.getContext(NETDB_DBCONN_CONTEXT, c);
        Pkt4Ptr query4_ptr;
        handle.getArgument("query4", query4_ptr);
        isc::asiolink::IOAddress subnet_id_addr = NULL;
        if (query4_ptr->isRelayed()) {
            auto dho_rai = query4_ptr->getOption(DHO_DHCP_AGENT_OPTIONS);
            if (dho_rai == nullptr || dho_rai->getOption(RAI_OPTION_LINK_SELECTION) == nullptr) {
                netdb_logger.error("No link selection option found");
                handle.setStatus(CalloutHandle::NEXT_STEP_DROP);
                return 0;
            }
            subnet_id_addr = isc::asiolink::IOAddress(
                dho_rai->getOption(RAI_OPTION_LINK_SELECTION)->getUint32());
        } else {
            subnet_id_addr = query4_ptr->getCiaddr();
        }
        pqxx::work txn{*c};
        pqxx::result r{
            txn.exec("CALL dhcpd.api_get_subnet(in_relay_ipaddr => " + txn.quote(subnet_id_addr.toText()) +
                     ", out_prefix=>'', out_prefix_len=>0, out_subnet_pk=>0);")
        };
        auto row = r[0];
        if (row["out_prefix"].is_null()) {
            netdb_logger.info("No subnet found for %1").arg(subnet_id_addr.toText());
            handle.setStatus(CalloutHandle::NEXT_STEP_DROP);
        } else {
            boost::shared_ptr<Subnet4> subnet(Subnet4::create(isc::asiolink::IOAddress(row["out_prefix"].c_str()),
                                                              row["out_prefix_len"].as<int>(), 0,
                                                              0, 120, row["out_subnet_pk"].as<uint64_t>()));
            auto pool = Pool4::create(isc::asiolink::IOAddress(row["out_prefix"].c_str()),
                                      isc::asiolink::IOAddress(
                                          isc::asiolink::IOAddress(row["out_prefix"].c_str()).toUint32() + 2 ^
                                          (32 - row["out_prefix_len"].as<int>())));
            subnet->addPool(pool);
            if (config->getCfgSubnets4()->getBySubnetId(subnet->getID()) != nullptr) {
                auto old_net = config->getCfgSubnets4()->getBySubnetId(subnet->getID())->get();
                if (!(old_net.first.equals(subnet->get().first) && old_net.second == subnet->get().second)) {
                    config->getCfgSubnets4()->replace(subnet);
                }
            } else {
                config->getCfgSubnets4()->add(subnet);
            }
            if (query4_ptr->getType() == DHCPREQUEST) {
                auto req_opt = query4_ptr->getOption(DHO_DHCP_REQUESTED_ADDRESS);
                isc::asiolink::IOAddress req_addr = NULL;
                if (req_opt == nullptr) {
                    req_addr = query4_ptr->getCiaddr();
                    if (req_addr.isV4Zero()) {
                        netdb_logger.error("No requested address option found");
                        handle.setStatus(CalloutHandle::NEXT_STEP_DROP);
                        return 0;
                    }
                } else {
                    req_addr = isc::asiolink::IOAddress(req_opt->getUint32());
                }
                auto fake_host = boost::make_shared<Host>(query4_ptr->getHWAddr()->toText(false), "hw-address",
                                                          subnet->getID(),
                                                          0, req_addr);
                handle.setContext(NETDB_FAKE_HOST_CONTEXT, fake_host);
                config->getCfgHosts()->add(fake_host);
            }
        }
        txn.commit();
    } catch (const pqxx::broken_connection &) {
        netdb_logger.error(ERR_DB_CONN);
        handle.setStatus(CalloutHandle::NEXT_STEP_DROP);
        return 1;
    }
    return (0);
}

int pkt4_send(CalloutHandle &handle) {
    Pkt4Ptr response4_ptr;
    handle.getArgument("response4", response4_ptr);
    try {
        OptionCollectionPtr tp;
        handle.getContext(NETDB_OPTION_CONTEXT, tp);
        response4_ptr->options_.clear();
        for (auto opt = tp->begin(); opt != tp->end(); ++opt) {
            if (response4_ptr->getOption(opt->first) != nullptr) {
                netdb_logger.warn("DB BUG: Duplicate option %1! Ignoring second occurrence...").arg(opt->first);
                continue;
            }
            response4_ptr->addOption(opt->second);
        }
    } catch (const NoSuchCalloutContext &) {
        handle.setStatus(CalloutHandle::NEXT_STEP_DROP);
    }
    return (0);
}

int lease4_select(CalloutHandle &handle) {
    try {
        boost::shared_ptr<pqxx::connection> c;
        handle.getContext(NETDB_DBCONN_CONTEXT, c);
        Pkt4Ptr query4_ptr;
        handle.getArgument("query4", query4_ptr);
        HWAddrPtr hwaddr_ptr = query4_ptr->getHWAddr();
        bool renew = false;
        bool fake_allocation;
        try {
            handle.getArgument("fake_allocation", fake_allocation);
        } catch (const NoSuchArgument &) {
            netdb_logger.debug(10, "DHCPRENEW detected");
        }
        if (query4_ptr->getType() == 9) {
            renew = true;
        }
        pqxx::nontransaction txn{*c};
        std::string opt_str = "{";
        for (auto opt = query4_ptr->options_.begin(); opt != query4_ptr->options_.end(); ++opt) {
            std::stringstream first_hex;
            // Postgres doesn't like odd numbers of digits
            first_hex << std::setw(2) << std::setfill('0') << std::hex << opt->first;
            opt_str += "{\\\\x";
            opt_str += first_hex.str();
            opt_str += ",\\\\x";
            opt_str += opt->second->toHexString(false).erase(0, 2);
            opt_str += "}";
            if (std::next(opt) != query4_ptr->options_.end()) {
                opt_str += ",";
            }
        }
        opt_str += "}";
        auto dho_rai = query4_ptr->getOption(DHO_DHCP_AGENT_OPTIONS);
        std::optional<std::basic_string<char> > relay = std::nullopt;
        if (dho_rai && dho_rai->getOption(RAI_OPTION_LINK_SELECTION)) {
            relay = isc::asiolink::IOAddress(
                dho_rai->getOption(RAI_OPTION_LINK_SELECTION)->getUint32()).toText();
        }
        Lease4Ptr lease4_ptr;
        handle.getArgument("lease4", lease4_ptr);
        OptionCollectionPtr tp;
        handle.getContext(NETDB_OPTION_CONTEXT, tp);
        try {
            pqxx::result r;
            if (query4_ptr->getType() == DHCPDISCOVER) {
                if (!relay) {
                    netdb_logger.error("DISCOVER: Missing relay address!");
                    handle.setStatus(CalloutHandle::NEXT_STEP_SKIP);
                    return 1;
                }
                c->prepare("dhcpd.api_discover",
                           "CALL dhcpd.api_discover(in_relay_ipaddr => $1, in_mac_addr => $2 , in_client_options => $3::bytea[], out_offer_ipaddr => '', out_lease_options => array[]::bytea[]);");
                r = txn.exec_prepared("dhcpd.api_discover", relay, hwaddr_ptr->toText(false), opt_str);
            } else if (query4_ptr->getType() == DHCPREQUEST || renew) {
                netdb_logger.debug(10, "Entering REQUEST-Mode...");
                std::optional<std::basic_string<char> > ciaddr = std::nullopt;
                if (!query4_ptr->getCiaddr().isV4Zero()) {
                    ciaddr = query4_ptr->getCiaddr().toText();
                }
                c->prepare("dhcpd.api_request",
                           "CALL dhcpd.api_request(in_client_ipaddr => $1, in_relay_ipaddr => $2, in_mac_addr => $3, in_client_options => $4::bytea[], out_offer_ipaddr => '', out_lease_options => array[]::bytea[]);");
                if (!ciaddr && !relay) {
                    netdb_logger.error("No ciaddr and no relay address found");
                    handle.setStatus(CalloutHandle::NEXT_STEP_SKIP);
                    return 1;
                }
                r = txn.exec_prepared("dhcpd.api_request", ciaddr,
                                      relay, hwaddr_ptr->toText(false),
                                      opt_str);
            }
            if (auto row = r[0]; row["out_offer_ipaddr"].is_null()) {
                std::cout << "No lease found";
                if (!renew && fake_allocation) {
                    std::cout << " :: discover_mode" << std::endl;
                } else {
                    std::cout << " :: request_mode" << std::endl;
                }
                std::cout << "in_client_options: " << opt_str << std::endl;
                if (!fake_allocation) {
                    if (row["out_lease_options"].is_null()) {
                        std::cout << "out_lease_options is null" << std::endl;
                        handle.setStatus(CalloutHandle::NEXT_STEP_DROP);
                    } else {
                        std::cout << "out_lease_options is NOT null" << std::endl;
                        extract_options(tp, row);
                    }
                    lease4_ptr->addr_ = NULL;
                    handle.setArgument("lease4", lease4_ptr);
                } else {
                    handle.setStatus(CalloutHandle::NEXT_STEP_SKIP);
                }
            } else {
                std::cout << "Offer from NETDB " << row["out_offer_ipaddr"] << std::endl;
                lease4_ptr->addr_ = isc::asiolink::IOAddress(row["out_offer_ipaddr"].c_str());
                handle.setArgument("lease4", lease4_ptr);
                extract_options(tp, row);
            }
            txn.commit();
        } catch (const pqxx::sql_error &e) {
            std::cout << "SQL error: " << e.what() << std::endl;
            std::cout << "Query was: " << e.query() << std::endl;
            std::cout << "SQL state: " << e.sqlstate() << std::endl;
            std::cout << "in_client_options: " << opt_str << std::endl;
            txn.abort();
            handle.setStatus(CalloutHandle::NEXT_STEP_DROP);
        }
    } catch (const pqxx::broken_connection &) {
        netdb_logger.error(ERR_DB_CONN);
        handle.setStatus(CalloutHandle::NEXT_STEP_SKIP);
        return 1;
    }
    return (0);
}

int lease4_decline(CalloutHandle &handle) {
    try {
        boost::shared_ptr<pqxx::connection> c;
        handle.getContext(NETDB_DBCONN_CONTEXT, c);
        Pkt4Ptr query4_ptr;
        handle.getArgument("query4", query4_ptr);
        auto req_opt = query4_ptr->getOption(DHO_DHCP_REQUESTED_ADDRESS);
        std::optional<std::basic_string<char> > req_ip_str = std::nullopt;
        if (!req_opt) {
            req_ip_str = isc::asiolink::IOAddress(req_opt->getUint32()).toText();
        }
        HWAddrPtr hwaddr_ptr = query4_ptr->getHWAddr();
        const auto relay = isc::asiolink::IOAddress(
            query4_ptr->getOption(DHO_DHCP_AGENT_OPTIONS)->getOption(RAI_OPTION_LINK_SELECTION)->getUint32());
        pqxx::nontransaction txn{*c};
        c->prepare("dhcpd.api_decline",
                   "CALL dhcpd.api_decline(in_relay_ipaddr => $1, in_requested_ipaddr => $2, in_mac_addr => $3);");
        auto r = txn.exec_prepared("dhcpd.api_decline", relay.toText(), req_ip_str, hwaddr_ptr->toText(false));
    } catch (const pqxx::broken_connection &) {
        netdb_logger.error(ERR_DB_CONN);
        handle.setStatus(CalloutHandle::NEXT_STEP_SKIP);
        return 1;
    }
    return (0);
}

int lease4_server_decline(CalloutHandle &handle) {
    return lease4_decline(handle);
}

int lease4_renew(CalloutHandle &handle) {
    return lease4_select(handle);
}

int lease4_release(CalloutHandle &handle) {
    try {
        boost::shared_ptr<pqxx::connection> c;
        handle.getContext(NETDB_DBCONN_CONTEXT, c);
        Pkt4Ptr query4_ptr;
        handle.getArgument("query4", query4_ptr);
        std::optional<std::basic_string<char> > clip_str = std::nullopt;
        if (!query4_ptr->getCiaddr().isV4Zero()) {
            clip_str = query4_ptr->getCiaddr().toText();
        }
        c->prepare("dhcpd.api_release",
                   "CALL dhcpd.api_release(in_client_ipaddr => $1, in_mac_addr => $2);");
        pqxx::nontransaction txn{*c};
        txn.exec_prepared("dhcpd.api_release", clip_str, query4_ptr->getHWAddr()->toText(false));
    } catch (const pqxx::broken_connection &) {
        netdb_logger.error(ERR_DB_CONN);
        handle.setStatus(CalloutHandle::NEXT_STEP_SKIP);
        return 1;
    }
    return (0);
}
}
