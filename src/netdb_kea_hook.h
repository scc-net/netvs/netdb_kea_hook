#ifndef DHCP_NETDB_KEA_NETDB_KEA_HOOK_H
#define DHCP_NETDB_KEA_NETDB_KEA_HOOK_H

#include <log/message_initializer.h>
#include <log/macros.h>

namespace packet_capture {
    extern isc::log::Logger packet_capture_logger;
}

#endif //DHCP_NETDB_KEA_NETDB_KEA_HOOK_H
